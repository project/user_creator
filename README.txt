
Description
===========
Allows users with the permission "create limited users" create users
without having to give them full user administration.

Site administrators can select which roles can create which other roles.

Ex. Principal could be configured to create both Teacher and Student
accounts, and Teacher could be configured to create student accounts
but not other teacher accounts or principal accounts.


Installation
============

1. Enable the module.

2. Create at least two additional roles.

3. Give permissions to at least one of those roles to "create limited users".

4. Give permissions to the same role to "access administration pages".

5. Go to the module settings for User Creator, and define who can create who.

6. Edit the menu to move the Add User link where you like.


Contributor
============
Elliott Rothman superelliott@thesupergroup.com

Development of this module is sponsored by The SuperGroup
www.thesupergroup.com